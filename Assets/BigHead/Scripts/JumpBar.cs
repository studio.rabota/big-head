﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JumpBar : MonoBehaviour {

	public Image ImgHealthBar;

	public float Min;

	public float Max;

	private float mCurrentValue;

	private float mCurrentPercent;

	private void Start () {
		ImgHealthBar.fillAmount = 0;
	}

	public void SetValue(float health)
	{
		if(health != mCurrentValue)
		{
			if(Max - Min == 0)
			{
				mCurrentValue = 0;
				mCurrentPercent = 0;
			}
			else
			{

				mCurrentValue = health;
				mCurrentPercent = (float)mCurrentValue / (float)(Max - Min) - 1;
			}
				
			ImgHealthBar.fillAmount = mCurrentPercent;
		}
	}

	public float CurrentPercent
	{
		get { return mCurrentPercent; }
	}

	public float CurrentValue
	{
		get { return mCurrentValue;  }
	}

}
