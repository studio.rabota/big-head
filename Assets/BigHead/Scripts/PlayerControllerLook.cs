﻿
// Place on the first rock
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerLook : MonoBehaviour {

	#region Private Members

	private float Gravity = 20.0f;

	private Animator _animator;
	private CharacterController _characterController;

	private Vector3 moveDirection = Vector3.zero;
	private Vector3 lookAtTarget;
	private Quaternion playerRotation;
	private bool afterJumping = false;
	private float jumpSpeedPressed;

	private PointsIndicator PointsIndicator;
	private JumpBar JumpBar;
	private HUD HudComponent;
	
	#endregion

	#region Public Members

	public HUD Hud;
	public Route Route;

	public float rotationSpeed = 5.0f;
	public float jumpSpeed = 5.0f;
	public float jumpSpeedAmplifier = 0.1f;
	public float jumpSpeedMax = 10f;

	public bool isDead = false;

	public AudioClip jumpClip;
	public AudioClip endOfGameClip;

	private string messageBeginGame = "Hold Space to jump higher";
	private string messageRestart = "Press Enter to restart";

	#endregion

	void Start () {
		_animator = GetComponent<Animator>();
		_characterController = GetComponent<CharacterController>();

		// Set jumpSpeedPressed to base jumpSpeed
		jumpSpeedPressed = jumpSpeed;

		// HUD 
		HudComponent = Hud.GetComponent<HUD>();

		// UI
		JumpBar = Hud.transform.Find("Bars_Panel/JumpBar").GetComponent<JumpBar>();
		JumpBar.Min = jumpSpeed;
		JumpBar.Max = jumpSpeedMax;

		// Points
		PointsIndicator = Hud.transform.Find("Bars_Panel/PointsIndicator").GetComponent<PointsIndicator>();

		// Set up the start game
		InitGame();
	}

	void InitGame () {
		// Start a new route
		Route.StartRoute ();

		// Start on the first rock
		Route.Jumped ();
		Vector3 currentPosition = Route.GetCurrentRouteObject();
		transform.position = currentPosition;

		// Player is alive
		isDead = false;
		_animator.SetTrigger("alive");

		// Set points to zero
		PointsIndicator.BeginGamePoints();

		// Look at the first rock
		Vector3 nextPosition = Route.GetNextRouteObject();
		lookAt (nextPosition);

		// Show message 
		HudComponent.OpenAndCloseMessage(messageBeginGame, 6);
	}

	void Update () {
		// Jump
		if (_characterController.isGrounded) {
			if (Input.GetButton("Jump") && isDead == false)
			{
				IncreasejumpSpeed();
			} else if (Input.GetButtonUp("Jump") && isDead == false)
			{
				Jump ();
			} else if (afterJumping)
			{
				AfterJump ();
			}
		}

		// Restart game
		if (Input.GetButton("Restart") && isDead)
		{
			HudComponent.CloseMessagePanel ();
			InitGame();
		}

		// Rotate player
		transform.rotation = Quaternion.Slerp (transform.rotation,
			playerRotation,
			rotationSpeed * Time.deltaTime
		);

		// Come down by gravity
		moveDirection.y -= Gravity * Time.deltaTime;

		_characterController.Move(moveDirection * Time.deltaTime);
	}

	private void AfterJump () {
		// Stop forward movement
		moveDirection = transform.forward * 0;

		// Set jumpSpeedPressed back to base jumpSpeed
		jumpSpeedPressed = jumpSpeed;

		// Turn off Big Head's "in air" position 
		_animator.SetBool("is_in_air", false);

		// Look at the next rock
		Vector3 nextPosition = Route.GetNextRouteObject ();
		lookAt (nextPosition);

		// Set afterJumping to false - so AfterJump is called only once
		afterJumping = false;

		// Reset jump bar ui
		JumpBar.SetValue(0);
	}

	private void Jump() {
		// Animate to in air pos
		_animator.SetBool("is_in_air", true);

		// Play audioClip
		AudioSource.PlayClipAtPoint(jumpClip, transform.position);

		// Forward
		moveDirection = transform.forward * jumpSpeedPressed;

		// Jump
		moveDirection.y = jumpSpeedPressed;

		// Tell the Route - "Player jumped"
		Route.Jumped ();

		// Set jump bar ui to indicate jumpSpeed
		JumpBar.SetValue(jumpSpeedPressed);

		// Set afterJumping
		afterJumping = true;
	}
		
	private void IncreasejumpSpeed()
	{
		if (jumpSpeedMax > jumpSpeedPressed) {
			// Increased jump speed 
			jumpSpeedPressed += jumpSpeedAmplifier;

			// Show jump speed in ui
			JumpBar.SetValue(jumpSpeedPressed);
		}
	}
		
	private void Death () {
		isDead = true;
		_animator.SetTrigger("death");
		AudioSource.PlayClipAtPoint (endOfGameClip, Camera.main.transform.position, 1f);
		HudComponent.OpenMessagePanel (messageRestart);
	}

	private void lookAt (Vector3 position) {
		lookAtTarget = position - transform.position;
		lookAtTarget.y = 0;
		playerRotation = Quaternion.LookRotation(lookAtTarget);
	}

	void OnControllerColliderHit(ControllerColliderHit hit){
		if (hit.gameObject.tag != "LandingArea" && isDead == false) {
			Death ();
		} 
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "LandingArea") {
			PointsIndicator.AddPoints (1);
		}
	}

}
