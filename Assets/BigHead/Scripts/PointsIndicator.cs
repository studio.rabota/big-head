﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsIndicator : MonoBehaviour {

	public Text TxtPoints;

	public int StartWithPoints = 0;
	private int Points = 0;

	private void Start () {
		BeginGamePoints ();
	}

	public void AddPoints(int addedPoints)
	{
		Points = Points + addedPoints;

		TxtPoints.text = string.Format("{0}", Mathf.RoundToInt(Points));
	}

	public void BeginGamePoints () {
		Points = StartWithPoints;
	}

}
