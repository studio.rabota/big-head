﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Route : MonoBehaviour {

	#region Public Members

	public JumpOnObject[] JumpOnObjects;
	public int StartOnIndex = 0;
	private int Jumps = 0;

	#endregion

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public Vector3 GetCurrentRouteObject () {
		return JumpOnObjects [StartOnIndex + (Jumps - 1)].transform.Find("Landing").transform.position;
	}

	public Vector3 GetNextRouteObject () {
 		return JumpOnObjects [StartOnIndex + Jumps].transform.Find("Landing").transform.position;
	}

	public void StartRoute () {
		Jumps = StartOnIndex;
	}

	public void Jumped () {
		Jumps++;

		// No more rocks left - go back to the first rock
		if (Jumps == JumpOnObjects.Length) {
			Jumps = StartOnIndex;
		}
	}
}
